import React from 'react';
import styled from 'styled-components';


    
const Titulo = styled.h1`
color: red;
border: 1px solid red;
padding: 5px;
`;

const Bola = styled.div`
width: 50px;
height: 50px;
border-radius: 50px;
background-color: ${(props) => props.color || "green"};
display: inline-block;
margin: 5px;
`;

const FotoBorde = styled.img`
    border: 1px solid red;
    padding: 5px;
`;



export default () => {

    return (
        <>
            <Titulo>Hola soy un componente</Titulo>
            <FotoBorde src="http://placekitten.com/200/200" />
            <Bola color="blue" />
            <Bola color="red" />
         
            <Bola />
        </>
    )
}