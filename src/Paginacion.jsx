import React, { useState } from 'react';



export default (props) => {

    const [valor, setValor] = useState(props.min);

    const menos = () => {
      if (valor > props.min) {
        setValor(valor - 1);
      }
    };
  
    const mas = () => {
      if (valor < props.max - props.elementos + 1) {
        setValor(valor + 1);
      }
    };

    const lis = [];

    for (let i = 0; i < props.elementos; i++) {
      lis.push(<li key={i} className="page-item" onClick={()=>props.setPagina(valor+i)} > <a className="page-link" href="#" >{valor + i}</a></li>);
    }


    return (


      <ul className="pagination">
      <li className="page-item" onClick={menos}>
        <a className="page-link" href="#" aria-label="Previous">
          <span aria-hidden="true" >&laquo;</span>
        </a>
      </li>

      {lis}

      <li className="page-item" onClick={mas}>
        <a className="page-link" href="#" aria-label="Next">
          <span aria-hidden="true" >&raquo;</span>
        </a>
      </li>
    </ul>
    )

}