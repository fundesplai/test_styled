import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import Componente from './Compo';
import Marcador from './Marcador';

import Display from './DisplayContador';
import Boton from './BtnContador';
import Paginacion from './Paginacion';

import { LIMITE_SUPERIOR, LIMITE_INFERIOR, ITEMS } from './Config';


function App() {

  const [valor, setValor] = useState(LIMITE_INFERIOR);
  const [modal, setModal] = useState(false);
  const [mensajeModal, setMensajeModal] = useState('');

  const menos = () => {
    if (valor > LIMITE_INFERIOR) {
      setValor(valor - 1);
    }
  };

  const mas = () => {
    if (valor < LIMITE_SUPERIOR - ITEMS + 1) {
      setValor(valor + 1);
    }
  };

  const botones = [];

  for (let i = 0; i < ITEMS; i++) {
    botones.push(<Boton key={i} mensaje={valor + i} alerta={"alerta " + i} />)
  }


  // const elementos = [1,2,3];
  // const botones = elementos.map(el => <Boton key={el} mensaje={valor+el} />);



  const toggle = () => setModal(!modal);
  
  const setPagina = (num) => {
    setMensajeModal("Estas en la página "+num);
    setModal(!modal);
  }


  return (
    <>
      {/* <Boton accion={menos} mensaje="-" />
      {botones}
      <Boton accion={mas} mensaje="+" /> */}

      <Paginacion elementos={4} min={10} max={20} setPagina={setPagina} />
 

      <Modal isOpen={modal} toggle={toggle} >
        <ModalHeader toggle={toggle}>Paginación</ModalHeader>
        <ModalBody>
          {mensajeModal}
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={toggle}>Salir</Button>
        </ModalFooter>
      </Modal>

    </>

  );
}

export default App;

/*
  <ul class="pagination">
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
*/