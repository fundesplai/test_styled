import React, {useState} from 'react';
import { Tooltip, Button } from 'reactstrap';



export default (props) => {
    const [tooltipOpen, setTooltipOpen] = useState(false);
    const toggle = () => setTooltipOpen(!tooltipOpen);

    return (
        <>
        <Button id="TooltipExample" onClick={props.accion} >{props.mensaje}</Button>
        <Tooltip placement="right" isOpen={tooltipOpen} target="TooltipExample" toggle={toggle}>
            {props.alerta}
        </Tooltip>
        </>
    )
}